import os
import sys
sys.path.append('/home/satt/venv/')
sys.path.append('/home/satt/projects/laika/')
sys.path.append('/home/satt/projects/laika/laika')
with open("/home/satt/venv/bin/activate_this.py") as f:
    code = compile(f.read(), "/home/satt/venv//bin/activate_this.py", "exec")
    exec(code, dict(__file__="/home/satt/venv//bin/activate_this.py"))
os.environ['DJANGO_SETTINGS_MODULE'] = 'laika.settings'

django.core.wsgi import get_wsgi_application
from whitenoise.django import DjangoWhiteNoise
application = DjangoWhiteNoise(get_wsgi_application())
