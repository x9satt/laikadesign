﻿# -*- coding: utf-8 -*-
#coding=utf-8
from django.contrib import admin
from django.forms import ModelForm
from .widgets import MultiFileInput
from .models import Project, Sketch, Visualization, Implementation, SketchImage, VisualizationImage, ImplementationImage, IndexSketchImage
# Register your models here.


class SketchImagesInLine(admin.TabularInline):
    model = SketchImage
    extra = 3
    

class VisualizationImagesInLine(admin.TabularInline):
    model = VisualizationImage
    extra = 3
    
    
class ImplementationImagesInLine(admin.TabularInline):
    model = ImplementationImage
    extra = 3
    

class ProjectAdmin(admin.ModelAdmin):
    list_display = ('is_Active', 'project_name', 'created_at', 'updated_at',)
    list_display_links = ('project_name',)
    list_per_page = 20
    ordering = ['is_Active', '-updated_at']
    search_fields = ['project_name', 'description',]
    exclude = ('created_at', 'updated_at',)

#    prepopulated_fields = {'slug':('category_name',)}


class SketchAdmin(admin.ModelAdmin):
    inlines = [SketchImagesInLine]
    list_display = ('project', 'meta_keywords', 'created_at', 'updated_at',)
    list_display_links = ('project',)
    list_per_page = 20
    ordering = ['-updated_at']
    search_fields = ['project', 'meta_keywords', 'meta_description']
    exclude = ('created_at', 'updated_at',)
    
    prepopulated_fields = {'sketch_slug':('project','sketchword',)}
    
    
class VisualizationAdmin(admin.ModelAdmin):
    inlines = [VisualizationImagesInLine]
    list_display = ('project', 'meta_keywords', 'created_at', 'updated_at',)
    list_display_links = ('project',)
    list_per_page = 20
    ordering = ['-updated_at']
    search_fields = ['project', 'meta_keywords', 'meta_description']
    exclude = ('created_at', 'updated_at',)
    
    prepopulated_fields = {'visualization_slug':('project','visualizationword',)}

    
class ImplementationAdmin(admin.ModelAdmin):
    inlines = [ImplementationImagesInLine]
    list_display = ('project', 'meta_keywords', 'created_at', 'updated_at',)
    list_display_links = ('project',)
    list_per_page = 20
    ordering = ['-updated_at']
    search_fields = ['project', 'meta_keywords', 'meta_description']
    exclude = ('created_at', 'updated_at',)
    
    prepopulated_fields = {'implementation_slug':('project','implementationword',)}  
    
    
admin.site.register(Project, ProjectAdmin)
admin.site.register(Sketch, SketchAdmin)
admin.site.register(Visualization, VisualizationAdmin)
admin.site.register(Implementation, ImplementationAdmin)
admin.site.register(IndexSketchImage)