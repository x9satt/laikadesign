# -*- coding: utf-8 -*-
#coding=utf-8
from django.apps import AppConfig


class ProjectsConfig(AppConfig):
    name = 'projects'
