# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2016-12-21 12:33
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('projects', '0002_visualization'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sketch',
            name='sketchword',
            field=models.CharField(default='sketch', help_text='Дополнительное слово для Ссылки.</br>Extra word for sketch_slug.', max_length=18),
        ),
        migrations.AlterField(
            model_name='visualization',
            name='visualizationword',
            field=models.CharField(default='visualization', help_text='Дополнительное слово для Ссылки.</br>Extra word for visualization_slug.', max_length=18),
        ),
    ]
