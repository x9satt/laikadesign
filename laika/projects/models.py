﻿# -*- coding: utf-8 -*-
#coding=utf-8
from django.db import models
import uuid
from django.db.models.signals import pre_delete
from django.dispatch.dispatcher import receiver

def get_file_path(instance, filename):
    ext = filename.split('.')[-1]
    filename = "%s.%s" % (uuid.uuid4(), ext)
    return 'static/img/%s/%s/%s' % (filename[:1], filename[2:3], filename)



class Project(models.Model):
    project_name = models.CharField(max_length=255, unique=True, primary_key=True, help_text='Уникальное(для раздела эскизы) значение имени проекта.</br>Unique(in sketch section) project\'s name value.')
    is_Active = models.BooleanField(default=True, help_text='Флаг показа.</br>Displaying flag.')
    description = models.TextField(help_text='Описание проекта.</br>Description of the project.')
    short_description = models.TextField(help_text='Короткое описание проекта. Для показа при наведении.</br> Short description of the project shown while hover.')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    
    def __str__(self):
        return self.project_name    
        
    class Meta:
        db_table = 'projects'
        ordering = ['-updated_at']
        verbose_name = 'Проект'
        verbose_name_plural = 'Проекты'

        
class Sketch(models.Model):
    project = models.OneToOneField(Project, related_name='sketch', help_text='Выберите проект. Проект должен быть создан заранее.</br>Choose project. Project should exist.')
    sketch_slug = models.SlugField(max_length=255, unique=True, 
        help_text = 'Уникальное значение для Ссылки эскиза, созданное из имени проекта.</br>Unique value for sketch page URL, created from project\'s name.')
    sketchword = models.CharField(max_length=18, default="sketch", help_text='Дополнительное слово для Ссылки.</br>Extra word for sketch_slug.')

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    meta_keywords = models.TextField(help_text='Разделённые запятыми слова\фразы для поисковой оптимизации.</br>Comma-delimited set of SEO keywords for meta tag.')
    meta_description = models.TextField(help_text='Описание для поисковой оптимизации.</br>Content for description meta tag.')

    preview = models.ImageField(blank=False, upload_to=get_file_path, 
        help_text='Изображение для показа на главной странице или в списке проектов.</br>Preview for the main page or projects list.')#, default='static/shop/images/previews/default.png', upload_to=get_file_path)
    
    class Meta:
        db_table = 'sketches'
        ordering = ['-updated_at']
        verbose_name = 'Эскиз'
        verbose_name_plural = 'Эскизы'
        
    def __str__(self):
        return self.project.project_name
        
    @models.permalink
    def get_absolute_url(self):
        return ('projects:show_sketch', (), { 'sketch_slug': self.sketch_slug })


@receiver(pre_delete, sender=Sketch)
def Sketch_delete(sender, instance, **kwargs):
    instance.preview.delete(False)
    
    
class SketchImage(models.Model):
    sketch = models.ForeignKey(Sketch, related_name='images', on_delete=models.CASCADE)
    image = models.ImageField(blank=True, upload_to=get_file_path)

@receiver(pre_delete, sender=SketchImage)
def SketchImage_delete(sender, instance, **kwargs):
    instance.image.delete(False)
    

class IndexSketchImage(models.Model):
    image = models.ImageField(blank=False, upload_to=get_file_path)

@receiver(pre_delete, sender=IndexSketchImage)
def IndexSketchImage_delete(sender, instance, **kwargs):
    instance.image.delete(False)

    
class Visualization(models.Model):
    project = models.OneToOneField(Project, related_name='visualization', help_text='Выберите проект. Проект должен быть создан заранее.</br>Choose project. Project should exist.')
    visualization_slug = models.SlugField(max_length=255, unique=True, 
        help_text = 'Уникальное значение для Ссылки эскиза, созданное из имени проекта.</br>Unique value for visualization page URL, created from project\'s name.')
    visualizationword = models.CharField(max_length=18, default="visualization", help_text='Дополнительное слово для Ссылки.</br>Extra word for visualization_slug.')

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    meta_keywords = models.TextField(help_text='Разделённые запятыми слова\фразы для поисковой оптимизации.</br>Comma-delimited set of SEO keywords for meta tag.')
    meta_description = models.TextField(help_text='Описание для поисковой оптимизации.</br>Content for description meta tag.')

    preview = models.ImageField(blank=False, upload_to=get_file_path, 
        help_text='Изображение для показа на главной странице или в списке проектов.</br>Preview for the main page or projects list.')#, default='static/shop/images/previews/default.png', upload_to=get_file_path)
    
    class Meta:
        db_table = 'visualizations'
        ordering = ['-updated_at']
        verbose_name = 'Визуализация'
        verbose_name_plural = 'Визуализации'
        
    def __str__(self):
        return self.project.project_name
        
    @models.permalink
    def get_absolute_url(self):
        return ('projects:show_visualization', (), { 'visualization_slug': self.visualization_slug })


@receiver(pre_delete, sender=Visualization)
def Visualization_delete(sender, instance, **kwargs):
    instance.preview.delete(False)
    
class VisualizationImage(models.Model):
    visualization = models.ForeignKey(Visualization, related_name='images', on_delete=models.CASCADE)
    image = models.ImageField(blank=True, upload_to=get_file_path)

@receiver(pre_delete, sender=VisualizationImage)
def VisualizationImage_delete(sender, instance, **kwargs):
    instance.image.delete(False)

    
class Implementation(models.Model):
    project = models.OneToOneField(Project, related_name='implementation', help_text='Выберите проект. Проект должен быть создан заранее.</br>Choose project. Project should exist.')
    implementation_slug = models.SlugField(max_length=255, unique=True, 
        help_text = 'Уникальное значение для Ссылки эскиза, созданное из имени проекта.</br>Unique value for implementation page URL, created from project\'s name.')
    implementationword = models.CharField(max_length=18, default="implementation", help_text='Дополнительное слово для Ссылки.</br>Extra word for implementation_slug.')

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    meta_keywords = models.TextField(help_text='Разделённые запятыми слова\фразы для поисковой оптимизации.</br>Comma-delimited set of SEO keywords for meta tag.')
    meta_description = models.TextField(help_text='Описание для поисковой оптимизации.</br>Content for description meta tag.')

    preview = models.ImageField(blank=False, upload_to=get_file_path, 
        help_text='Изображение для показа на главной странице или в списке проектов.</br>Preview for the main page or projects list.')
        
    
    class Meta:
        db_table = 'implementations'
        ordering = ['-updated_at']
        verbose_name = 'Реализация'
        verbose_name_plural = 'Реализации'
        
    def __str__(self):
        return self.project.project_name
        
    @models.permalink
    def get_absolute_url(self):
        return ('projects:show_implementation', (), { 'implementation_slug': self.implementation_slug })


@receiver(pre_delete, sender=Implementation)
def Implementation_delete(sender, instance, **kwargs):
    instance.preview.delete(False)
    
    
class ImplementationImage(models.Model):
    implementation = models.ForeignKey(Implementation, related_name='images', on_delete=models.CASCADE)
    image = models.ImageField(blank=True, upload_to=get_file_path)


@receiver(pre_delete, sender=ImplementationImage)
def ImplementationImage_delete(sender, instance, **kwargs):
    instance.image.delete(False)
