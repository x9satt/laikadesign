from django import template
import random
from projects.models import Project, Visualization, Sketch, Implementation, IndexSketchImage

register = template.Library()

# первые три не нужны, такой же код внесен во views.py.
@register.inclusion_tag("tags/last_five_projects.html")
def last_five_projects(request_path):
    #last_five = Visualization.objects.all()[0:5]
    #last_five = Project.objects.filter(is_Active=True)[0:5]
    #active_projects = Project.objects.filter(is_Active=True)
	
    
    # последние визуализации
    #active_vis = []
    #last_five_vis = []
    #for i in active_projects:
    #    if hasattr(i, 'visualization'):
            #last_five_vis.append('dsf')
    #        active_vis.append(i.visualization)
    #last_five_vis = active_vis[0:5]
    #firstRand = random.randint(0, len(last_five_vis)-1)
    #secondRand = random.randint(0, len(last_five_vis)-1)
    #while secondRand == firstRand:
    #    secondRand = random.randint(0, len(last_five_vis)-1)
    return {
    #    'last_five':last_five,
        'request_path':request_path,
    #    'last_five_vis':last_five_vis[firstRand],
    #    'firstRand':last_five_vis[firstRand],
    #    'secondRand':last_five_vis[secondRand],
    }

    
@register.inclusion_tag("tags/last_five_sketches.html")
def last_five_sketches(request_path):
    active_projects = Project.objects.filter(is_Active=True)
	
    # последние эскизы
    active_sket = []
    last_five_sket = []
    for i in active_projects:
        if hasattr(i, 'sketch'):
            active_sket.append(i.sketch)
    last_five_sket = active_sket[0:5]
    rand = random.randint(0, len(last_five_sket)-1)
        
    return {
        'request_path':request_path,
        'last_five_sket':last_five_sket[rand]
    }

    
@register.inclusion_tag("tags/index_sketch_images.html")
def indexSketchImages(request_path):
    images = IndexSketchImage.objects.all()
    firstRandomImage = random.randint(0, len(images)-1)
    secondRandomImage = random.randint(0, len(images)-1)
    while secondRandomImage == firstRandomImage:
        secondRandomImage = random.randint(0, len(images)-1)
    return {
        'firstRandomImage':images[firstRandomImage].image,
        'secondRandomImage':images[secondRandomImage].image,
        'request_path':request_path
    }
    
@register.inclusion_tag("tags/sketch_list.html")
def sketch_list(request_path):
    active_projects = Project.objects.filter(is_Active=True)
    active_sketches = []
    for i in active_projects:
        if hasattr(i, 'sketch'):
            active_sketches.append(i.sketch)
        
    return {
        'active_sketches':active_sketches,
        'request_path':request_path
    }
    
    
@register.inclusion_tag("tags/visualization_list.html")
def visualization_list(request_path):
    active_projects = Project.objects.filter(is_Active=True)
    active_visualizations = []
    for i in active_projects:
        if hasattr(i, 'visualization'):
            active_visualizations.append(i.visualization)
        
    return {
        'active_visualizations':active_visualizations,
        'request_path':request_path
    }
    

@register.inclusion_tag("tags/implementation_list.html")
def implementation_list(request_path):
    active_projects = Project.objects.filter(is_Active=True)
    active_implementations = []
    for i in active_projects:
        if hasattr(i, 'implementation'):
            active_implementations.append(i.implementation)
        
    return {
        'active_implementations':active_implementations,
        'request_path':request_path
    }
#@register.inclusion_tag("tags/item_list.html")
#def item_list(request_path):
 #   active_items = Item.objects.filter(is_active=True)
  #  return {
   #     'active_items':active_items,
    #    'request_path':request_path
    #}