﻿# -*- coding: utf-8 -*-
#coding=utf-8
from django.conf.urls import url
from . import views
from django.conf import settings
from django.conf.urls.static import static

app_name = 'projects'
urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^visualization$', views.VisView, {'template_name':'projects/visualization.html'}, name='visview'),
    url(r'^implementation$', views.ImpView, {'template_name':'projects/implementation.html'}, name='impview'),
    url(r'^sketch$', views.SkView, {'template_name':'projects/sketch.html'}, name='skview'),
    url(r'^sketch/(?P<sketch_slug>[-\w]+)/$', views.show_sketch, {'template_name':'projects/project.html'}, name='show_sketch'),
    url(r'^implementation/(?P<implementation_slug>[-\w]+)/$', views.show_implementation, {'template_name':'projects/project.html'}, name='show_implementation'),
    url(r'^visualization/(?P<visualization_slug>[-\w]+)/$', views.show_visualization, {'template_name':'projects/project.html'}, name='show_visualization'),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)