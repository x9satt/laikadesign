﻿# -*- coding: utf-8 -*-
#coding=utf-8
from django.shortcuts import render, get_object_or_404, render_to_response, redirect
from django.template import RequestContext
import random
from .models import Sketch, Visualization, Implementation, Project, IndexSketchImage
from django.conf import settings
from django.core.mail import send_mail, EmailMultiAlternatives

from django.http import HttpResponse

site_name = settings.SITE_NAME


def index(request, template_name='projects/index.html'):
    if request.method == 'POST':
        postdata = request.POST.copy()
        mess_name = postdata.get('mess_name','')
        mess_email = postdata.get('mess_email','')
        mess_subject = 'Заявка для рассчёта стоимости проекта'
        mess_message = postdata.get('mess_message','')
        mess_phone = postdata.get('mess_phone','')
        mess_object = postdata.get('mess_object','')
        mess_area = postdata.get('mess_area','')
        mess_address = postdata.get('mess_address','')
        mess_from_email, mess_to = 'sender@laikadesign.ru', 'sender@laikadesign.ru'
        mess_text_content = ''
        mess_html_content = '<h3>Имя:</h3><p>' + mess_name +'</p><h3>Почта:</h3><p>'+ mess_email +'</p><h3>Телефон:</h3>\
            <p>' + mess_phone + '</p><h3>Объект:</h3><p>' + mess_object + '</p><h3>Площадь:</h3><p>' + mess_area + '</p><h3>Адрес:</h3>\
            <p>' + mess_address + '</p><br><h3>Сообщение:</h3><p>' + mess_message + '</p>'
        mess_msg = EmailMultiAlternatives(mess_subject, mess_text_content, mess_from_email, [mess_to])
        mess_msg.attach_alternative(mess_html_content, "text/html")
        mess_msg.send()
        return redirect(request.META['HTTP_REFERER'])

    # последние визуализации
    last_five = Project.objects.filter(is_Active=True)[0:5]
    active_projects = Project.objects.filter(is_Active=True)
    active_vis = []
    last_five_vis = []
    for i in active_projects:
        if hasattr(i, 'visualization'):
            active_vis.append(i.visualization)
    last_five_vis = active_vis[0:5]
    firstRand = random.randint(0, len(last_five_vis)-1)
    secondRand = random.randint(0, len(last_five_vis)-1)
    while secondRand == firstRand:
        secondRand = random.randint(0, len(last_five_vis)-1)
        
    #рандомные картинки    
    images = IndexSketchImage.objects.all()
    firstRandomImage = random.randint(0, len(images)-1)
    secondRandomImage = random.randint(0, len(images)-1)
    while secondRandomImage == firstRandomImage:
        secondRandomImage = random.randint(0, len(images)-1)
    context = {
        'site_name':site_name,
        'location':'index',
        'firstRand':last_five_vis[firstRand],
        'secondRand':last_five_vis[secondRand],
        'firstRandomImage':images[firstRandomImage].image,
        'secondRandomImage':images[secondRandomImage].image,
    }
    return render(request, template_name, context)

def prices(request, template_name='projects/prices.html'):
    context = {'site_name':site_name, 'location':'price'}
    return render(request, template_name, context)
    
def show_sketch(request, sketch_slug, template_name='projects/project.html'):
    p = get_object_or_404(Sketch, sketch_slug=sketch_slug)
    page_title = p.project
    meta_keywords = p.meta_keywords
    meta_description = p.meta_description
    back = 'skview'
    context = {'p':p,'page_title':page_title,'meta_keywords':meta_keywords,'meta_description':meta_description, 'site_name':site_name, 'back':back}
    return render(request, template_name, context)

    
def SkView(request, template_name='projects/sketch.html'):
    page_title = 'Эскизы'
    meta_keywords = 'Эскизы проектов'
    meta_description = 'Эскизы проектов Laikadesign'
    context = {'page_title':page_title,'meta_keywords':meta_keywords,'meta_description':meta_description, 'site_name':site_name, 'location':'ske'}
    return render(request, template_name, context)
    

def show_visualization(request, visualization_slug, template_name='projects/project.html'):
    p = get_object_or_404(Visualization, visualization_slug=visualization_slug)
    page_title = p.project
    meta_keywords = p.meta_keywords
    meta_description = p.meta_description
    back = "visview"
    context = {'p':p,'page_title':page_title,'meta_keywords':meta_keywords,'meta_description':meta_description, 'site_name':site_name, 'back':back}
    return render(request, template_name, context)
    
    
def VisView(request, template_name='projects/visualization.html'):
    page_title = 'Визуализации'
    meta_keywords = 'Визуализации проектов'
    meta_description = 'Визуализации проектов Laikadesign'
    context = {'page_title':page_title,'meta_keywords':meta_keywords,'meta_description':meta_description, 'site_name':site_name, 'location':'vis'}
    return render(request, template_name, context)

    
def show_implementation(request, implementation_slug, template_name='projects/project.html'):
    p = get_object_or_404(Implementation, implementation_slug=implementation_slug)
    page_title = p.project
    meta_keywords = p.meta_keywords
    meta_description = p.meta_description
    back = "impview"
    context = {'p':p,'page_title':page_title,'meta_keywords':meta_keywords,'meta_description':meta_description, 'site_name':site_name, 'back':back}
    return render(request, template_name, context)
    
def ImpView(request, template_name='projects/implementation.html'):
    page_title = 'Реализации'
    meta_keywords = 'Реализации проектов'
    meta_description = 'Реализации проектов Laikadesign'
    context = {'page_title':page_title,'meta_keywords':meta_keywords,'meta_description':meta_description, 'site_name':site_name, 'location':'imp'}
    return render(request, template_name, context)
    
    
def team(request, template_name='projects/team.html'):
    context = {'site_name':site_name, 'location':'team'}
    return render(request, template_name, context)

    
def ContactsView(request, template_name="projects/contacts.html"):
    if request.method == 'POST':
        postdata = request.POST.copy()
        mess_name = postdata.get('mess_name','')
        mess_email = postdata.get('mess_email','')
        mess_phone = postdata.get('mess_phone','')
        mess_subject = postdata.get('mess_subject','')
        mess_message = postdata.get('mess_message','')
        mess_from_email, mess_to = 'sender@laikadesign.ru', 'sender@laikadesign.ru'
        mess_text_content = ''
        mess_html_content = '<h3>Имя:</h3><p>' + mess_name +'</p><h3>Почта:</h3><p>'+ mess_email +'</p><h3>Телефон:</h3>\
            <p>' + mess_phone + '</p><br><h3>Сообщение:</h3><p>' + mess_message + '</p>'
        mess_msg = EmailMultiAlternatives(mess_subject, mess_text_content, mess_from_email, [mess_to])
        mess_msg.attach_alternative(mess_html_content, "text/html")
        mess_msg.send()
        return redirect(request.META['HTTP_REFERER'])
    page_title = 'Контакты'
    meta_keywords = 'Контакты, Laikadesign, Адрес Laikadesign, Телефоны Laikadesign, телефон laikadesign'
    meta_description = 'Контакты Laikadesign'
    context = {'page_title':page_title,'meta_keywords':meta_keywords,'meta_description':meta_description, 'site_name':site_name, 'location':'cont'}
    return render(request, template_name, context)